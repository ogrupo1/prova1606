#!/bin/bash

hp=$(python3 -c "from random import randint; print(randint(100,200))")
hp=$(($hp + 16))
dg=$(python3 -c "from random import randint; print(randint(100,5000))")

function ataca(){

	#gera o dano que o jogador vai dar
	dnp=$(python3 -c "from random import randint; print(randint(10, 100))")
	#gera o numero de vezes que o dragao ataca
	at=$(python3 -c "from random import randint; print(randint(1,5))")
	#gera o dano que o dragao dará
	dnd=$(python3 -c "from random import randint; print(randint(1,10))")
	#causa o dano ao dragao
	dg=$(( $dg - $dnp ))
	#gera o dano total que o dragao causará
	dnd=$(( $dnd * $at ))
	hp=$(( $hp - $dnd ))
	echo Você deu $dnp de dano!
	echo Dagrao causou $dnd de dano em troca!

}

function fugir(){
	fugiu=$(python3 -c "from random import randint; print(randint(1,10))")
	test $fugiu -gt 2 && echo Dagrão tacou-lhe uma baforada e você foi derrotado! || echo Você meteu o pé o conseguiu escapar do temido Dagrão!
}

function curar(){
	curou=$(python3 -c "from random import randint; print(randint(50,100))")	
	hp=$(( $hp + $curou ))
	fugiu=$python3 -c "from random import randint; print(randint(1,10))"
	test "$fugiu" = "1" && echo Dagrao fugiu!!! || echo Dagrao tentou fugir, mas não conseguiu!
}

function info(){
	echo Vida do Player=$hp
	echo Vida do Dagrão=$dg
}

clear
echo O terrível dragão Dagrão apareceu!!
#sleep 3

function menu(){
	printf "Escolha uma opção!\n\n[1] Atacar\n[2] Fugir\n[3] Curar\n[4] Info"
	read acao
	case $acao in
		1)
			ataca
			;;
		2)
			fugir
			exit
			;;
		3)
			curar
			;;
		4)
			info
			;;	
		*)
			echo Opção Inválida! Olhe, o Dagrão vai te pegar hein!	
	esac
}

while true ;
do
	test $dg -le 0 && test $hp -le 0 && echo O Dagrão foi derrotado, mas você foi junto! :\( && exit
	test $dg -le 0 && echo O terrível Dagrão foi derrotado, você venceu! && exit
	test $hp -le 0 && echo Você foi derrotado pela besta Dagrão! && break
	menu
	echo "#################################"
done
