#!/bin/bash

hp=$1
dg=$2

clear
echo O terrível dragão Dagrão apareceu!!
sleep 3

function menu(){
	printf "Escolha uma opção!\n\n[1]Atacar\n[2]Fugir\n[3]Info\n"
	read acao
	case $acao in
		1)
			hp=$(($hp-10))
			dg=$(($dg-100))
			echo Você deu 100 de dano no Dagrão e recebeu 10 de volta!
			;;
		2)
			fugiu=$(python3 -c "from random import randint; print(randint(1,2))")
			echo $fugiu
			test "$fugiu" = "1" && echo Você meteu o pé o conseguiu escapar do temido Dagrão! || echo Dagrão tacou-lhe uma baforada e você foi derrotado!
			exit
			;;
		3)
			echo Vida do Player= $hp
			echo Vida do Dagrão= $dg
			;;	
		*)
			echo Opção Inválida! Olhe, o Dagrão vai te pegar hein!	
	esac
}

while true ;
do
	test $dg -le 0 && test $hp -le 0 && echo O Dagrão foi derrotado, mas você foi junto! :\( && exit
	test $dg -le 0 && echo O terrível Dagrão foi derrotado, você venceu! && exit
	test $hp -le 0 && echo Você foi derrotado pela besta Dagrão! && break
	menu
	echo "#################################"
done
